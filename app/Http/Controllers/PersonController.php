<?php

namespace App\Http\Controllers;

use App\Person;
use Illuminate\Contracts\Validation\Factory;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class PersonController extends BaseController
{
    const VALIDATIONS = [
        'firstname' => 'required|max:99',
        'surname'   => 'required|max:99'
    ];


    public function add(Request $request)
    {
        $this->validate($request, self::VALIDATIONS);

        $person = new Person();
        $person->firstname = $request->firstname;
        $person->surname = $request->surname;
        $person->setUpdatedAt(null);

        if ($person->save()) {
            return response()->json(['id' => $person->id], 201, ['location' => '/persons/' . $person->id]);
        }

        abort(500, 'There was a error creating the new person');
    }


    public function update(Request $request, $id)
    {
        $person = Person::find($id);

        if ($person !== null) {
            $validator = $this->getValidationFactory();
            if ($request->has('firstname')) {
                $this->patchValidation($validator, $request, ['firstname' => self::VALIDATIONS['firstname']]);
                $person->firstname = $request->firstname;
            }
            if ($request->has('surname')) {
                $this->patchValidation($validator, $request, ['surname' => self::VALIDATIONS['surname']]);
                $person->surname = $request->surname;
            }

            if($person->save()) {
                return response()->json([], 204);
            }

            abort(500, 'There was a error updating the person');
        }
    }


    public function delete($id)
    {
        $person = Person::find($id);

        if ($person !== null) {
            if($person->delete()) {
                return response()->json([], 204);
            }
            abort(500, 'There was a error deleting the person');
        }
        abort(404, 'Person not found');
    }


    public function fetch($id)
    {
        $person = Person::find($id);

        if ($person !== null) {
            return $person->makeHidden(['updated_at', 'deleted_at', 'id']);
        }

        abort(404, 'Person not found');
    }


    private function patchValidation(Factory $validator, Request $request, array $pairValidation)
    {
        $field = key($pairValidation);
        $rule = $pairValidation[$field];
        $validator = $validator->make($request->only($field), [
            $field => $rule
        ]);

        if ($validator->fails()) {
            $this->throwValidationException($request, $validator);
        }
    }
}