<?php

namespace App\Http\Middleware;

use Closure;

class AcceptOnlyJsonRequestMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->isJson()) {
            abort(406, 'Content-Type header accepted only: application/json');
        }

        return $next($request);
    }
}
