 # CRUD-OPERATIONS-LUMEN
It's a API sample made with LUMEN.  
Execute CRUD operations over a entity(PERSON).  


### Install Dependencies
`composer install`


### Testing
A functional test file is found in: tests/PersonsTest.php  
The test is responsible for:  
  - Creating the DB struct
  - Populating the DB with fake data
  - Starting the PHP built-in server
  - Execute CRUD tests

#### Run Tests     
vendor/bin/phpunit`


### HTTP Verb Conventions Used
- Create a resource:
    - POST Request
    - Response:
      - Header: 
        - 201: Along with location pointing to the new resource(success)
      - Body: 
        - json object with the new id.
        - If we need extra information of the resource make a new get request

- Replace a complete resource:
    - PUT Request with the new info/data for replace the specific resource
    - Response:
      - Header:
        - 204: Not body content(For success)
      - Body:
        - Empty

- Delete a resource
    - DELETE Request
    - Response:
      - Headers: 
        - 204: Not body content(For success).
        - 404: After the resource has been deleted and there's a new delete request
      - Body: 
        - In both cases Empty

- Update a partial resource or make operations over resource
    - PATCH Request
    - Response:
      - Header: 
        - 204: Not body content(For success).
      - Body:
        - Empty


### API Routes
| HTTP Method | Route         | Controller    | Description           |
| ------------|---------------|---------------|-----------------------|
| POST        | /persons      | Person:add    | Create a new person   |
| GET         | /persons/{id} | Person:fetch  | Fetch a person by id  |
| PATCH       | /persons/{id} | Person:update | Update a person by id | 
| DELETE      | /persons/{id} | Person:delete | Delete a person by id |

They are defined in: routes/web.php  
Controller is defined in: Http/Controllers/PersonController.php  
Only calls with the header application/json are accepted. It's defined in: Http/Middleware/AcceptOnlyJsonRequestMiddleware.php


### Data Structure
SQLite file: database/crud-operations-lumen.sqlite
```
CREATE TABLE persons (
  id         INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT,
  firstname  VARCHAR  NOT NULL,
  surname    VARCHAR  NOT NULL,
  created_at DATETIME NOT NULL,
  updated_at DATETIME,
  deleted_at DATETIME
);
```
As the data structure layer is abstract we can migrate to Postgres, MariaDB, SQL Server just specifying the DB settings(.env file).  
The schema is located in: 2018_02_04_230956_initial_schema.php  
Personally migrate a database system is not as easy as many tools claims, but in most the cases they help.  
  
### Logs and Error Handling
When an unexpected error arise or an intentional exception(ex: abort() function) is throw a new log entry is created.  
Logs are found in storage/logs/lumen.log 