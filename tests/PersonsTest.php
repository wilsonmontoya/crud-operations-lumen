<?php

use AssociativeAssertions\AssociativeAssertions as AA;

class PersonsTest extends TestCase
{
    use \AssociativeAssertions\AssociativeArrayTrait;

    private static $baseURL = 'localhost:8765';
    protected      $baseUrl = 'localhost:8765';


    public static function setUpBeforeClass()
    {
        exec('php artisan migrate:refresh');
        exec('php artisan db:seed');
        $cmdPHPServer = 'php -S ' . self::$baseURL . ' -t public/ > /dev/null &';
        exec($cmdPHPServer);
    }


    public static function tearDownAfterClass()
    {
        $phpServerPID = shell_exec("ps -aux | grep '[ ]" . self::$baseURL . "' | awk '{print $2}'");
        exec('kill -9 ' . $phpServerPID);
    }


    public function testAddPerson()
    {
        $personData = [
            'firstname' => 'Bob',
            'surname'   => 'Martins'
        ];


        $this->json('POST', '/persons/', $personData);
        $this->assertResponseStatus(201);
        // Test if the response matches the structure
        $response = json_decode($this->response->getContent(), true);
        $this->assertAssociativeArray(['id' => AA::assertInt()], $response);
        $this->assertRegExp('~/persons/\d+~', $this->response->headers->get('location'));
    }


    public function testGetPerson()
    {
        $this->json('GET', '/persons/3');
        $this->assertResponseStatus(200);
        $this->seeJsonEquals(['firstname' => 'Alex', 'surname' => 'Hammond', 'created_at' => '2018-02-09 11:11:11']);
    }


    public function testUpdatePerson()
    {
        $patchData = ['surname' => '안녕하세요'];
        $this->json('PATCH', '/persons/3', $patchData);
        $this->assertResponseStatus(204);

        $this->json('GET', '/persons/3');
        $this->seeJsonEquals(['firstname' => 'Alex', 'surname' => '안녕하세요', 'created_at' => '2018-02-09 11:11:11']);
    }


    public function testDeletePerson()
    {
        $this->json('DELETE', '/persons/5');
        $this->assertResponseStatus(204);
        // If the person exists in table, then softDelete feature worked
        $this->seeInDatabase('persons', ['id' => 5]);

        $this->json('GET', '/persons/5');
        $this->assertResponseStatus(404);
    }
}