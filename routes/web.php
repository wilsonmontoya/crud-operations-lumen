<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


/** @var \Laravel\Lumen\Routing\Router $router */
$router->get('/', function () use ($router) {
    return ['version' => '0.9.9'];
});

$router->group(['prefix' => 'persons'], function () use ($router) {
    $router->post('/', 'PersonController@add');
    $router->patch('{id}', 'PersonController@update');
    $router->get('{id}', 'PersonController@fetch');
    $router->delete('{id}', 'PersonController@delete');
});