<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = date('Y-m-d H:i:s');
        DB::table('persons')->insert(
            [
                [
                    'firstname'  => 'Jeff',
                    'surname'    => 'Stelling',
                    'created_at' => $date
                ],
                [
                    'firstname'  => 'Chris',
                    'surname'    => 'Kamara',
                    'created_at' => $date
                ],
                [
                    'firstname'  => 'Alex',
                    'surname'    => 'Hammond',
                    'created_at' => '2018-02-09 11:11:11'
                ],[
                    'firstname'  => 'Jim',
                    'surname'    => 'White',
                    'created_at' => $date
                ],[
                    'firstname'  => 'Natalie',
                    'surname'    => 'Sawyer',
                    'created_at' => $date
                ],
            ]
        );
    }
}
